# A Laravel client for sending SMS via SMSMKT.

[![Latest Version on Packagist](https://img.shields.io/packagist/v/cjworx/laravel-smsmkt.svg?style=flat-square)](https://packagist.org/packages/cjworx/laravel-smsmkt)
[![Total Downloads](https://img.shields.io/packagist/dt/cjworx/laravel-smsmkt.svg?style=flat-square)](https://packagist.org/packages/cjworx/laravel-smsmkt)

This package is a wrapper around SMSMKT client for Laravel.

## Installation

You can install the package via composer:

```bash
composer require cjworx/laravel-smsmkt
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="smsmkt-config"
```

## Usage

### Add your SMSMKT credentials into .env file

```env
SMSMKT_API_KEY=
SMSMKT_SECRET_KEY=
SMSMKT_OTP_PROJECT_KEY=
SMSMKT_SENDER_NAME=
```

### Example of basic usage

```php
<?php

use CJWORX\LaravelSMSMKT\SMSMKTClient;

// Send SMS
SMSMKTClient::send('0811111111', 'Hello World!');

// Send OTP
$token = SMSMKTClient::sendOTP('0811111111');

// Verify OTP
$verified = SMSMKTClient::verifyOTP($token, '123456');
```

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
