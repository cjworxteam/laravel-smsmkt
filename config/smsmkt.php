<?php

// config for CJWORX/LaravelSMSMKT
return [
  'host' => env('SMSMKT_HOST', 'https://portal-otp.smsmkt.com'),
  'api_key' => env('SMSMKT_API_KEY', ''),
  'secret_key' => env('SMSMKT_SECRET_KEY', ''),
  'sender_name' => env('SMSMKT_SENDER_NAME', 'Laravel'),
  'otp_project_key' => env('SMSMKT_OTP_PROJECT_KEY', ''),
];
