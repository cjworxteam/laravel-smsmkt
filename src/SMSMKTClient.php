<?php

namespace CJWORX\LaravelSMSMKT;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SMSMKTClient
{
  public static function send(string $phone, string $message)
  {
    // Send SMS
    $response = Http::withHeaders([
      'api_key' => config('smsmkt.api_key'),
      'secret_key' => config('smsmkt.secret_key')
    ])->post(config('smsmkt.host') . '/api/send-message', [
      'message' => $message,
      'phone' => $phone,
      'sender' => config('smsmkt.sender_name')
    ]);

    // Request failed
    if ($response->failed()) {
      Log::error('Failed to send SMS: [' . $response->getStatusCode() . '] ' . $response->getReasonPhrase());

      abort(500, 'Failed to send SMS');
    }

    // Request error
    if ($response['code'] != '000') {
      $errorDetail = $response['detail'];

      if ($errorDetail) Log::error('Failed to send SMS: ' . $errorDetail);
      else Log::error('Failed to send SMS');

      abort(500, 'Failed to send SMS');
    }

    return $response;
  }

  public static function sendOTP(string $phone)
  {
    $token = null;

    // Send OTP (returns token)
    $response = Http::withHeaders([
      'api_key' => config('smsmkt.api_key'),
      'secret_key' => config('smsmkt.secret_key')
    ])->post(config('smsmkt.host') . '/api/otp-send', [
      'project_key' => config('smsmkt.otp_project_key'),
      'phone' => $phone,
    ]);

    // Request failed
    if ($response->failed()) {
      Log::error('Failed to send OTP: [' . $response->getStatusCode() . '] ' . $response->getReasonPhrase());

      abort(500, 'เกิดข้อผิดพลาดในการส่ง OTP');
    }

    // Request error
    if ($response['code'] != '000') {
      $errorDetail = $response['detail'];

      if ($errorDetail) Log::error('Failed to send OTP: ' . $errorDetail);
      else Log::error('Failed to send OTP');

      abort(500, 'เกิดข้อผิดพลาดในการส่ง OTP');
    }

    // Set token
    if ($response['result']) {
      $token = $response['result']['token'];
    }

    // Token error
    if (!$token) {
      Log::error('Failed to send OTP: No token responded.');

      abort(500, 'เกิดข้อผิดพลาดในการส่ง OTP');
    }

    return $token;
  }

  public static function verifyOTP(string $token, string $otpCode)
  {
    $response = Http::withHeaders([
      'api_key' => config('smsmkt.api_key'),
      'secret_key' => config('smsmkt.secret_key')
    ])->post(config('smsmkt.host') . '/api/otp-validate', [
      'token' => $token,
      'otp_code' => $otpCode,
    ]);

    // Request failed
    if ($response->failed()) {
      Log::error('Failed to verify OTP: [' . $response->getStatusCode() . '] ' . $response->getReasonPhrase());

      abort(500, 'เกิดข้อผิดพลาดในการตรวจสอบ OTP');
    }

    // Request error
    if ($response['code'] != '000') {
      return false;
    }

    return $response['result']['status'];
  }
}
